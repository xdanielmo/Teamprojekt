package de.ostfalia.learncoaching.request;

import de.ostfalia.learncoaching.entity.*;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class ProtokollRequest {

	private Protokoll protokoll;
	private List<Anliegen> anliegen;
	//private MultipartFile file;


	
//	public MultipartFile getFile() {
//		return file;
//	}
//
//	public void setFile(MultipartFile file) {
//		this.file = file;
//	}

	public Protokoll getProtokoll() {
		return protokoll;
	}

	public void setProtokoll(Protokoll protokoll) {
		this.protokoll = protokoll;
	}

	public List<Anliegen> getAnliegen() {
		return anliegen;
	}

	public void setAnliegen(List<Anliegen> anliegen) {
		this.anliegen = anliegen;
	}
}
