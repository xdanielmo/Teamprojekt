package de.ostfalia.learncoaching.request;

import de.ostfalia.learncoaching.entity.ErfahrenDurch;
import de.ostfalia.learncoaching.entity.Erfahrungen;
import de.ostfalia.learncoaching.entity.Kunde;

public class KundeRequest {

	private ErfahrenDurch erfahrenDurch;
	private Erfahrungen erfahrungen;
	private Kunde kunde;

	public ErfahrenDurch getErfahrenDurch() {
		return erfahrenDurch;
	}

	public void setErfahrenDurch(ErfahrenDurch erfahrenDurch) {
		this.erfahrenDurch = erfahrenDurch;
	}

	public Erfahrungen getErfahrungen() {
		return erfahrungen;
	}

	public void setErfahrungen(Erfahrungen erfahrungen) {
		this.erfahrungen = erfahrungen;
	}

	public Kunde getKundendaten() {
		return kunde;
	}

	public void setKundendaten(Kunde kunde) {
		this.kunde = kunde;
	}
}
