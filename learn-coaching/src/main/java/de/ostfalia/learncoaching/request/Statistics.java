package de.ostfalia.learncoaching.request;

import java.util.List;

public class Statistics {

	private long kundenAnzahl;
	private long anzahlBeratungen;
	private List<AnzhalBeratungenProKunde> anzhalBeratungenProKunde;
	private List<AnliegenStatistics> anliegen;
	private long manner;
	private long frauen;
	private long durchnittsalter;

	public interface AnzhalBeratungenProKunde {
		String getId();

		long getCount();
	}

	public interface AnliegenStatistics {
		String getName();

		long getCount();
	}

	public long getKundenAnzahl() {
		return kundenAnzahl;
	}

	public void setKundenAnzahl(long kundenAnzahl) {
		this.kundenAnzahl = kundenAnzahl;
	}

	public long getAnzahlBeratungen() {
		return anzahlBeratungen;
	}

	public void setAnzahlBeratungen(long anzahlBeratungen) {
		this.anzahlBeratungen = anzahlBeratungen;
	}

	public List<AnzhalBeratungenProKunde> getAnzhalBeratungenProKunde() {
		return anzhalBeratungenProKunde;
	}

	public void setAnzhalBeratungenProKunde(List<AnzhalBeratungenProKunde> anzhalBeratungenProKunde) {
		this.anzhalBeratungenProKunde = anzhalBeratungenProKunde;
	}

	public List<AnliegenStatistics> getAnliegen() {
		return anliegen;
	}

	public void setAnliegen(List<AnliegenStatistics> anliegen) {
		this.anliegen = anliegen;
	}

	public long getManner() {
		return manner;
	}

	public void setManner(long manner) {
		this.manner = manner;
	}

	public long getFrauen() {
		return frauen;
	}

	public void setFrauen(long frauen) {
		this.frauen = frauen;
	}

	public long getDurchnittsalter() {
		return durchnittsalter;
	}

	public void setDurchnittsalter(long durchnittsalter) {
		this.durchnittsalter = durchnittsalter;
	}
}
