package de.ostfalia.learncoaching;

import java.sql.Date;

import org.springframework.web.multipart.MultipartFile;

public class Geschpräch {

	private String sonstiges;

	private MultipartFile file;

	private Date datum;

	private Integer jahr;

	public String getSonstiges() {
		return sonstiges;
	}

	public void setSonstiges(String sonstiges) {
		this.sonstiges = sonstiges;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public Date getDatum() {
		return datum;
	}

	public void setDatum(Date datum) {
		this.datum = datum;
	}

	public Integer getJahr() {
		return jahr;
	}

	public void setJahr(Integer jahr) {
		this.jahr = jahr;
	}

}
