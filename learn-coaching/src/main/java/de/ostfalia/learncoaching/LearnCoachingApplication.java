package de.ostfalia.learncoaching;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@EntityScan(basePackages = "de.ostfalia.learncoaching.entity")
@SpringBootApplication
@ComponentScan("de.ostfalia.*")
public class LearnCoachingApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearnCoachingApplication.class, args);
	}

}
