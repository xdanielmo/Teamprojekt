package de.ostfalia.learncoaching.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import de.ostfalia.learncoaching.entity.LogIn;

public interface LogInRepository extends JpaRepository<LogIn,String>{

	public int countByBenutzernameAndPasswort(String benutzername, String passwort);
	
	public int countByBenutzername(String benutzername);
	
	@Modifying
	@Transactional
	@Query(value="INSERT INTO logindaten (benutzername,passwort) VALUES (:benutzername,:passwort)",nativeQuery = true)
	public void addUser(@Param("benutzername")String benutzername, @Param("passwort") String passwort);
	
	@Modifying
	@Transactional
	@Query(value="DELETE FROM logindaten WHERE benutzername =: benutzername",nativeQuery = true)
	public void deleteUser(String benutzername, String passwort);
}
