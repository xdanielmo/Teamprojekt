package de.ostfalia.learncoaching.repository;

import de.ostfalia.learncoaching.entity.ErfahrenDurch;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ErfahrenDurchRepository extends JpaRepository<ErfahrenDurch, Integer>{

}
