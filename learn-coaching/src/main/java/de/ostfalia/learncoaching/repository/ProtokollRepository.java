package de.ostfalia.learncoaching.repository;

import de.ostfalia.learncoaching.entity.Protokoll;
import de.ostfalia.learncoaching.request.Statistics;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ProtokollRepository extends JpaRepository<Protokoll, Integer> {

	long countByJahr(Integer jahr);

	@Query("SELECT p.idKunde as id, COUNT(*) as count FROM Protokoll p GROUP BY p.idKunde, p.jahr having p.jahr=:jahr")
	List<Statistics.AnzhalBeratungenProKunde> findProtokollPerKundePerYear(Integer jahr);

	@Query(value = "SELECT p.datum, a.name, p.sonstiges FROM Protokoll p JOIN Anliegen a on p.idProtokoll = a.protokoll_idProtokoll WHERE p.idKunde=:id", nativeQuery = true)
	public String[] getProtokollen(Integer id);

	@Modifying
	@Transactional
	@Query(value = "UPDATE Protokoll SET sonstiges = null, media = null, datum = null, jahr = null WHERE idKunde =:id", nativeQuery = true)
	public void delete(int id);

}
