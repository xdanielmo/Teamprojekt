package de.ostfalia.learncoaching.repository;

import de.ostfalia.learncoaching.request.Statistics;
import org.springframework.data.jpa.repository.JpaRepository;

import de.ostfalia.learncoaching.entity.Anliegen;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AnliegenRepository extends JpaRepository<Anliegen, Integer>{

    @Query("SELECT name as name, COUNT(*) as count FROM Anliegen " +
            "GROUP BY name, jahr having jahr=:jahr")
    List<Statistics.AnliegenStatistics> findAnliegenPerYear(Integer jahr);

}
