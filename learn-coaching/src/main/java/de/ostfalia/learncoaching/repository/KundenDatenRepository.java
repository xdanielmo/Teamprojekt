package de.ostfalia.learncoaching.repository;

import de.ostfalia.learncoaching.entity.Kunde;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface KundenDatenRepository extends JpaRepository<Kunde, Integer> {

	long countByJahr(Integer jahr);

	long countByGeschlechtAndJahr(String geschlecht, Integer jahr);

	@Query("SELECT coalesce(AVG(alter), 0) FROM Kunde where jahr=:jahr")
	long averageAge(Integer jahr);

	Kunde findByVorNameAndNachName(String vorName, String NachName);

	@Modifying
	@Transactional
	@Query("update Kunde set email = null,regelstudienzeit = null, studiengang = null, semester = null, wohnort = null,"
			+ " vorName = null, nachName = null, telefon = null, alter_column = null where vorName = :vorName and nachName = :nachName")
	void deleteKunde(String vorName, String nachName);
}
