package de.ostfalia.learncoaching.repository;

import de.ostfalia.learncoaching.entity.Erfahrungen;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ErfahrungenRepository extends JpaRepository<Erfahrungen, Integer>{

}
