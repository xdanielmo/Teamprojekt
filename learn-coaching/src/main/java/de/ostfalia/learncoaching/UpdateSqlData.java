/*
package de.ostfalia.learncoaching;


import de.ostfalia.learncoaching.entity.Kunde;
import de.ostfalia.learncoaching.entity.Protokoll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class UpdateSqlData {

    @Autowired
    private DataSource dataSource;

    List<Kunde> kundenListe = new ArrayList<Kunde>();
    List<Hinweisgeber> hinweisgeberList = new ArrayList<Hinweisgeber>();
    List<String> aufmerksamSonstigesList = new ArrayList<String>();
    List<String> erfahrungSonstigesList = new ArrayList<String>();
    List<Protokoll> protokollList = new ArrayList<Protokoll>();

    int aufmerksamFlyer = 0;
    int aufmerksamRundmail = 0;
    int aufmerksamVorstellung = 0;
    int aufmerksamInternet = 0;
    int aufmerksamVortrag = 0;
    int aufmerksamKommilitonen = 0;

    int erfahrungAllgemein = 0;
    int erfahrungFachliche = 0;
    int erfahrungPsychotherapeutische = 0;
    int erfahrungSoziale = 0;
    int erfahrungCareer = 0;
    int erfahrungNichtGenannt = 0;

    public void main(String[] args) throws SQLException {
        updateList(Geschlecht.MALE, 2020);
        ausgabe();
    }

    public enum Geschlecht
    {
        MALE, FEMALE, NONE, ALL
    }

    */
/**
     * updated alle Listen und Variabeln
     * ruft dafür alle anderen Methoden auf
     * nur diese Methode öffnen reicht für ein komplettes update
     *
     * @param geschlecht
     * @param jahr
     * @throws SQLException
     *//*

    public void updateList(Geschlecht geschlecht, int jahr) throws SQLException {
        int id = 0;
        int alter = 0;
        int idErfahrenDurch = 0;
        int idErfahrungen = 0;
        String geschlechtEingabe = "";
        switch (geschlecht) {
            case MALE:
                geschlechtEingabe = "maennlich";
                break;
            case FEMALE:
                geschlechtEingabe = "weiblich";
                break;
            case NONE:
                geschlechtEingabe = "keine Angaben";
                break;
            case ALL:
                geschlechtEingabe = "alle";
                break;
        }
        final Connection connection = DataSourceUtils.getConnection(dataSource);
        if (!(geschlechtEingabe.equals("alle"))){
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Kunde WHERE geschlecht = ? AND jahr = ?;");
            statement.setString(1, geschlechtEingabe);
            statement.setInt(2, jahr);
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                id = (resultSet.getInt("idKunde"));
                alter = (resultSet.getInt("alter"));
                idErfahrenDurch = (resultSet.getInt("ErfahrenDurch_idErfahrenDurch"));
                idErfahrungen = (resultSet.getInt("Erfahrungen_idErfahrungen"));
                kundenListe.add(new Kunde(id, alter, geschlecht, idErfahrenDurch, idErfahrungen));
            }
        } else {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Kunde WHERE jahr = ?;");
            statement.setInt(1, jahr);
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                id = (resultSet.getInt("idKunde"));
                alter = (resultSet.getInt("alter"));
                idErfahrenDurch = (resultSet.getInt("ErfahrenDurch_idErfahrenDurch"));
                idErfahrungen = (resultSet.getInt("Erfahrungen_idErfahrungen"));
                kundenListe.add(new Kunde(id, alter, geschlecht, idErfahrenDurch, idErfahrungen));
            }
        }
        updateErfahrenDurch();
        updateErfahrungen();
        updateProtokoll(geschlechtEingabe, jahr);
        updateVariabel();
    }

    */
/**
     * Updated die ErfahrenDurch Werte
     * @throws SQLException
     *//*

    public void updateErfahrenDurch() throws SQLException {
        final Connection connection = DataSourceUtils.getConnection(dataSource);
        for (int i = 0; i < kundenListe.size(); i++) {
            final PreparedStatement statement = connection.prepareStatement("SELECT * FROM ErfahrenDurch WHERE idErfahrenDurch = ?;");
            statement.setInt(1, kundenListe.get(i).getIdErfahrenDurch());
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                    if ((resultSet.getInt("fyler") == 1)) {
                        kundenListe.get(i).setAufmerksamFlyer(true);
                    }
                    if ((resultSet.getInt("rundmail") == 1)) {
                        kundenListe.get(i).setAufmerksamRundmail(true);
                    }
                    if ((resultSet.getInt("vorstellung") == 1)) {
                        kundenListe.get(i).setAufmerksamVorstellung(true);
                    }
                    if ((resultSet.getInt("internet") == 1)) {
                        kundenListe.get(i).setAufmerksamInternet(true);
                    }
                    if ((resultSet.getInt("vortrag") == 1)) {
                        kundenListe.get(i).setAufmerksamVortrag(true);
                    }
                    if ((resultSet.getInt("kommilitoinnen") == 1)) {
                        kundenListe.get(i).setAufmerksamKommilitonen(true);
                    }
                    if (!(resultSet.getString("persoenlicherHinweis") == null)) {
                        addPersoenlicherHinweis(resultSet.getString("persoenlicherHinweis"));
                    }
                    if (!(resultSet.getString("sonstiges") == null)) {
                        aufmerksamSonstigesList.add(resultSet.getString("sonstiges"));
                    }
            }
            resultSet.close();
            statement.close();
        }
        connection.close();
    }

    */
/**
     * Updated die Erfahrungen Werte
     * @throws SQLException
     *//*

    public void updateErfahrungen() throws SQLException {
        final Connection connection = DataSourceUtils.getConnection(dataSource);
        for (int i = 0; i < kundenListe.size(); i++) {
            final PreparedStatement statement = connection.prepareStatement("SELECT * FROM Erfahrungen WHERE idErfahrungen = ?;");
            statement.setInt(1, kundenListe.get(i).getIdErfahrungen());
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                if ((resultSet.getInt("allgemeine") == 1)) {
                    kundenListe.get(i).setErfahrungAllgemein(true);
                }
                if ((resultSet.getInt("fachliche") == 1)) {
                    kundenListe.get(i).setErfahrungFachliche(true);
                }
                if ((resultSet.getInt("psychotherapeutische") == 1)) {
                    kundenListe.get(i).setErfahrungPsychotherapeutische(true);
                }
                if ((resultSet.getInt("soziale") == 1)) {
                    kundenListe.get(i).setErfahrungSoziale(true);
                }
                if ((resultSet.getInt("career") == 1)) {
                    kundenListe.get(i).setErfahrungCareer(true);
                }
                if ((resultSet.getInt("nichtGenannt") == 1)) {
                    kundenListe.get(i).setErfahrungNichtGenannt(true);
                }
                if (!(resultSet.getString("sonstiges") == null)) {
                    erfahrungSonstigesList.add(resultSet.getString("sonstiges"));
                }
            }
            resultSet.close();
            statement.close();
        }
        connection.close();
    }

    */
/**
     * Updated das Protokoll je nach eingegeben Geschlecht und Jahr
     * @param geschlecht
     * @param jahr
     * @throws SQLException
     *//*

    public void updateProtokoll(String geschlecht, int jahr) throws SQLException {
        List<Integer> idKundenList = new ArrayList<Integer>();
        List<Integer> idProtokollList = new ArrayList<Integer>();
        final Connection connection = DataSourceUtils.getConnection(dataSource);
        if (!(geschlecht.equals("alle"))) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Kunde WHERE geschlecht = ?;");
            statement.setString(1, geschlecht);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                idKundenList.add((resultSet.getInt("idKunde")));
            }
            for (int i = 0; i < idKundenList.size(); i++) {
                statement = connection.prepareStatement("SELECT * FROM Kunde_has_Protokoll WHERE Kunde_idKunde = ?;");
                statement.setInt(1, idKundenList.get(i));
                resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    idProtokollList.add((resultSet.getInt("Protokoll_idProtokoll")));
                }
            }
            for (int i = 0; i < idProtokollList.size(); i++) {
                statement = connection.prepareStatement("SELECT * FROM Protokoll WHERE YEAR (datum) = ? AND idProtokoll = ?;");
                statement.setInt(1, jahr);
                statement.setInt(2, idProtokollList.get(i));
                resultSet = statement.executeQuery();
                while (resultSet.next()) {
                    addAnliegen((resultSet.getString("Anliegen")));
                }
            }
        } else {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Protokoll WHERE YEAR (datum) = ?;");
            statement.setInt(1, jahr);
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                addAnliegen((resultSet.getString("Anliegen")));
            }
        }
    }

    */
/**
     * Fügt einen persoenlichen Hinweis hinzu und sorgt dafür, dass es keine doppelten Einträge gibt
     * @param name
     *//*

    public void addPersoenlicherHinweis(String name) {
        String cleanName = name.toLowerCase();
        boolean schranke = false;
        for (int i = 0; i < hinweisgeberList.size(); i++) {
            if (hinweisgeberList.get(i).getName().equals(cleanName)) {
                int anzahl = hinweisgeberList.get(i).getAnzahl();
                hinweisgeberList.get(i).setAnzahl(++anzahl);
                schranke = true;
            }
        }
        if (!schranke) {
            hinweisgeberList.add(new Hinweisgeber(cleanName, 1));
        }
    }

    */
/**
     * Fügt ein Anliegen hinzu und sorgt dafür, dass es keine doppelten Einträge gibt
     *
     * @param anliegen
     *//*

    public void addAnliegen(String anliegen) {
        boolean schranke = false;
        for (int i = 0; i < protokollList.size(); i++) {
            if (protokollList.get(i).getAnliegen().equals(anliegen)) {
                int anzahl = protokollList.get(i).getAnzahl();
                protokollList.get(i).setAnzahl(++anzahl);
                schranke = true;
            }
        }
        if (!schranke) {
            protokollList.add(new Protokoll(anliegen));
        }
    }

    */
/**
     * Updated die Variabeln (Aufmerksam und Erfahrungen)
     *//*

    public void updateVariabel() {
        for (int i = 0; i < kundenListe.size(); i++) {
            if (kundenListe.get(i).isAufmerksamFlyer()){
                aufmerksamFlyer++;
            }
            if (kundenListe.get(i).isAufmerksamRundmail()){
                aufmerksamRundmail++;
            }
            if (kundenListe.get(i).isAufmerksamVorstellung()){
                aufmerksamVorstellung++;
            }
            if (kundenListe.get(i).isAufmerksamInternet()){
                aufmerksamInternet++;
            }
            if (kundenListe.get(i).isAufmerksamVortrag()){
                aufmerksamVortrag++;
            }
            if (kundenListe.get(i).isAufmerksamKommilitonen()){
                aufmerksamKommilitonen++;
            }
            if (kundenListe.get(i).isErfahrungAllgemein()){
                erfahrungAllgemein++;
            }
            if (kundenListe.get(i).isErfahrungFachliche()){
                erfahrungFachliche++;
            }
            if (kundenListe.get(i).isErfahrungPsychotherapeutische()){
                erfahrungPsychotherapeutische++;
            }
            if (kundenListe.get(i).isErfahrungSoziale()){
                erfahrungSoziale++;
            }
            if (kundenListe.get(i).isErfahrungCareer()){
                erfahrungCareer++;
            }
            if (kundenListe.get(i).isErfahrungNichtGenannt()){
                erfahrungNichtGenannt++;
            }
        }
    }

    */
/**
     * Ausgabe der Daten zur Veranschaulichung und Testmöglichkeit
     * @throws SQLException
     *//*

    public void ausgabe() throws SQLException {
        System.out.println();
        System.out.println("-----------------------------------------");
        System.out.println("*Aufmerksam geworden durch*");
        System.out.println("-----------------------------------------");
        System.out.println();
        System.out.println("Flyer:");
        System.out.println(aufmerksamFlyer);
        System.out.println("Rundmail:");
        System.out.println(aufmerksamRundmail);
        System.out.println("Vorstellung:");
        System.out.println(aufmerksamVorstellung);
        System.out.println("Internet:");
        System.out.println(aufmerksamInternet);
        System.out.println("Vortrag:");
        System.out.println(aufmerksamVortrag);
        System.out.println("Kommilitonen:");
        System.out.println(aufmerksamKommilitonen);
        System.out.println();
        System.out.println("Hinweise von:");
        for (int i = 0; i < hinweisgeberList.size(); i++) {
            System.out.print("-"+hinweisgeberList.get(i).getName()+", ");
            System.out.print(hinweisgeberList.get(i).getAnzahl());
            System.out.println();
        }
        System.out.println();
        System.out.println("Durch Sonstiges:");
        for (int i = 0; i < aufmerksamSonstigesList.size(); i++) {
            System.out.println("-"+aufmerksamSonstigesList.get(i));
        }
        System.out.println();
        System.out.println("-----------------------------------------");
        System.out.println("*Erfahrungen mit*");
        System.out.println("-----------------------------------------");
        System.out.println();
        System.out.println("Allgemein:");
        System.out.println(erfahrungAllgemein);
        System.out.println("Fachliche:");
        System.out.println(erfahrungFachliche);
        System.out.println("Psychotherapeutische:");
        System.out.println(erfahrungPsychotherapeutische);
        System.out.println("Soziale:");
        System.out.println(erfahrungSoziale);
        System.out.println("Career:");
        System.out.println(erfahrungCareer);
        System.out.println();
        System.out.println("Durch Sonstiges:");
        for (int i = 0; i < erfahrungSonstigesList.size(); i++) {
            System.out.println("-"+erfahrungSonstigesList.get(i));
        }
        System.out.println();
        System.out.println("-----------------------------------------");
        System.out.println("*Anliegen*");
        System.out.println("-----------------------------------------");
        System.out.println();
        for (int i = 0; i < protokollList.size(); i++) {
            System.out.println(protokollList.get(i).getAnliegen());
            System.out.println("Anzahl: " + protokollList.get(i).getAnzahl());
            System.out.println();
        }
    }



}
*/
