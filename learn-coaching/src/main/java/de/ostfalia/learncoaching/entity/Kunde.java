package de.ostfalia.learncoaching.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Date;

import javax.persistence.*;

@Entity
@Table(name = "Kunde")
public class Kunde {

	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@Id
	@Column(name = "idKunde")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idKunde;

	@Column(name = "vorName")
	private String vorName;

	@Column(name = "nachName")
	private String nachName;

	@Column(name = "alter_column")
	private Integer alter;

	@Column(name = "wohnort")
	private String wohnort;

	@Column(name = "email")
	private String email;

	@Column(name = "telefon")
	private String telefon;

	@Column(name = "studiengang")
	private String studiengang;
	// @Column(name = "media")
	// private Image pfoto;

	@Column(name = "regelstudienzeit")
	private Integer regelstudienzeit;
	
	@Column(name = "semester")
	private Integer semester;

	@Column(name = "geschlecht")
	private String geschlecht;

	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@Column(name = "jahr")
	private Integer jahr;

	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@Column(name = "letzterEintrag")
	private Date letzterEintrag;

	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@Column(name = "ErfahrenDurch_idErfahrenDurch")
	private Integer erfahrenDurch_idErfahrenDurch;

	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@Column(name = "Erfahrungen_idErfahrungen")
	private Integer erfahrungen_idErfahrungen;

	public Integer getIdKunde() {
		return idKunde;
	}

	public void setIdKunde(Integer idKunde) {
		this.idKunde = idKunde;
	}

	public String getVorName() {
		return vorName;
	}

	public void setVorName(String vorName) {
		this.vorName = vorName;
	}

	public String getNachName() {
		return nachName;
	}

	public void setNachName(String nachName) {
		this.nachName = nachName;
	}

	public Integer getAlter() {
		return alter;
	}

	public void setAlter(Integer alter) {
		this.alter = alter;
	}

	public String getWohnort() {
		return wohnort;
	}

	public void setWohnort(String wohnort) {
		this.wohnort = wohnort;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}

	public String getStudiengang() {
		return studiengang;
	}

	public void setStudiengang(String studiengang) {
		this.studiengang = studiengang;
	}

	public Integer getRegelstudienzeit() {
		return regelstudienzeit;
	}

	public void setRegelstudienzeit(Integer regelstudienzeit) {
		this.regelstudienzeit = regelstudienzeit;
	}

	public String getGeschlecht() {
		return geschlecht;
	}

	public void setGeschlecht(String geschlecht) {
		this.geschlecht = geschlecht;
	}

	public Integer getJahr() {
		return jahr;
	}

	public void setJahr(Integer jahr) {
		this.jahr = jahr;
	}

	public Date getLetzterEintrag() {
		return letzterEintrag;
	}

	public void setLetzterEintrag(Date letzterEintrag) {
		this.letzterEintrag = letzterEintrag;
	}

	public Integer getErfahrenDurch_idErfahrenDurch() {
		return erfahrenDurch_idErfahrenDurch;
	}

	public void setErfahrenDurch_idErfahrenDurch(Integer erfahrenDurch_idErfahrenDurch) {
		this.erfahrenDurch_idErfahrenDurch = erfahrenDurch_idErfahrenDurch;
	}

	public Integer getErfahrungen_idErfahrungen() {
		return erfahrungen_idErfahrungen;
	}

	public void setErfahrungen_idErfahrungen(Integer erfahrungen_idErfahrungen) {
		this.erfahrungen_idErfahrungen = erfahrungen_idErfahrungen;
	}
	
	public Integer getSemester() {
		return semester;
	}

	public void setSemester(Integer semester) {
		this.semester = semester;
	}
}
