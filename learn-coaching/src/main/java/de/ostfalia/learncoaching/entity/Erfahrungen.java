package de.ostfalia.learncoaching.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "Erfahrungen")
public class Erfahrungen {

	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@Id
	@Column(name = "idErfahrungen")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idErfahrungen;

	@Column(name = "allgemeine")
	private Integer allgemeine;

	@Column(name = "fachliche")
	private Integer fachliche;

	@Column(name = "psychotherapeutische")
	private Integer psychotherapeutische;

	@Column(name = "soziale")
	private Integer soziale;

	@Column(name = "career")
	private Integer career;

	@Column(name = "nichtGenannt")
	private Integer nichtGenannt;

	@Column(name = "sonstiges")
	private String sonstiges;

	public Integer getIdErfahrungen() {
		return idErfahrungen;
	}

	public void setIdErfahrungen(Integer idErfahrungen) {
		this.idErfahrungen = idErfahrungen;
	}

	public Integer getAllgemeine() {
		return allgemeine;
	}

	public void setAllgemeine(Integer allgemeine) {
		this.allgemeine = allgemeine;
	}

	public Integer getFachliche() {
		return fachliche;
	}

	public void setFachliche(Integer fachliche) {
		this.fachliche = fachliche;
	}

	public Integer getPsychotherapeutische() {
		return psychotherapeutische;
	}

	public void setPsychotherapeutische(Integer psychotherapeutische) {
		this.psychotherapeutische = psychotherapeutische;
	}

	public Integer getSoziale() {
		return soziale;
	}

	public void setSoziale(Integer soziale) {
		this.soziale = soziale;
	}

	public Integer getCareer() {
		return career;
	}

	public void setCareer(Integer career) {
		this.career = career;
	}

	public Integer getNichtGenannt() {
		return nichtGenannt;
	}

	public void setNichtGenannt(Integer nichtGenannt) {
		this.nichtGenannt = nichtGenannt;
	}

	public String getSonstiges() {
		return sonstiges;
	}

	public void setSonstiges(String sonstiges) {
		this.sonstiges = sonstiges;
	}
}
