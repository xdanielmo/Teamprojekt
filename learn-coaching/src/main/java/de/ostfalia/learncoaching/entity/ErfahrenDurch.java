package de.ostfalia.learncoaching.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "ErfahrenDurch")
public class ErfahrenDurch {

	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@Id
	@Column(name = "idErfahrenDurch")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idErfahrenDurch;

	@Column(name = "fyler")
	private Integer fyler;

	@Column(name = "rundmail")
	private Integer rundmail;

	@Column(name = "vorstellung")
	private Integer vorstellung;

	@Column(name = "internet")
	private Integer internet;

	@Column(name = "vortrag")
	private Integer vortrag;

	@Column(name = "kommilitoinnen")
	private Integer kommilitoinnen;

	@Column(name = "persoenlicherHinweis")
	private String persoenlicherHinweis;

	@Column(name = "sonstiges")
	private String sonstiges;

	public Integer getIdErfahrenDurch() {
		return idErfahrenDurch;
	}

	public void setIdErfahrenDurch(Integer idErfahrenDurch) {
		this.idErfahrenDurch = idErfahrenDurch;
	}

	public Integer getFyler() {
		return fyler;
	}

	public void setFyler(Integer fyler) {
		this.fyler = fyler;
	}

	public Integer getRundmail() {
		return rundmail;
	}

	public void setRundmail(Integer rundmail) {
		this.rundmail = rundmail;
	}

	public Integer getVorstellung() {
		return vorstellung;
	}

	public void setVorstellung(Integer vorstellung) {
		this.vorstellung = vorstellung;
	}

	public Integer getInternet() {
		return internet;
	}

	public void setInternet(Integer internet) {
		this.internet = internet;
	}

	public Integer getVortrag() {
		return vortrag;
	}

	public void setVortrag(Integer vortrag) {
		this.vortrag = vortrag;
	}

	public Integer getKommilitoinnen() {
		return kommilitoinnen;
	}

	public void setKommilitoinnen(Integer kommilitoinnen) {
		this.kommilitoinnen = kommilitoinnen;
	}

	public String getPersoenlicherHinweis() {
		return persoenlicherHinweis;
	}

	public void setPersoenlicherHinweis(String persoenlicherHinweis) {
		this.persoenlicherHinweis = persoenlicherHinweis;
	}

	public String getSonstiges() {
		return sonstiges;
	}

	public void setSonstiges(String sonstiges) {
		this.sonstiges = sonstiges;
	}
}
