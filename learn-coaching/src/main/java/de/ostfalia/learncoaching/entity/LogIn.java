package de.ostfalia.learncoaching.entity;


import javax.persistence.*;

@Entity
@Table(name = "logindaten")
public class LogIn {
	
	@Id
	@Column(name = "benutzername")
	private String benutzername;

	@Column(name = "passwort")
	private String passwort;

	public String getBenutzername() {
		return benutzername;
	}

	public void setBenutzername(String benutzername) {
		this.benutzername = benutzername;
	}

	public String getPasswort() {
		return passwort;
	}

	public void setPasswort(String passwort) {
		this.passwort = passwort;
	}

	
}
