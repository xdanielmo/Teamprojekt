package de.ostfalia.learncoaching.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "Protokoll")
public class Protokoll {

	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@Id
	@Column(name = "idProtokoll")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idProtokoll;

	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@Column(name = "idKunde")
	private Integer idKunde;

	@Column(name = "sonstiges")
	private String sonstiges;

	@Lob
	@Column(name = "media")
	private byte[] media;

	@Column(name = "datum")
	private String datum;

	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@Column(name = "jahr")
	private Integer jahr;

	public Integer getIdProtokoll() {
		return idProtokoll;
	}

	public void setIdProtokoll(Integer idProtokoll) {
		this.idProtokoll = idProtokoll;
	}

	public Integer getIdKunde() {
		return idKunde;
	}

	public void setIdKunde(Integer idKunde) {
		this.idKunde = idKunde;
	}

	public String getSonstiges() {
		return sonstiges;
	}

	public void setSonstiges(String sonstiges) {
		this.sonstiges = sonstiges;
	}

	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}

	public Integer getJahr() {
		return jahr;
	}

	public void setJahr(Integer jahr) {
		this.jahr = jahr;
	}

	public byte[] getMedia() {
		return media;
	}

	public void setMedia(byte[] media) {
		this.media = media;
	}

}
