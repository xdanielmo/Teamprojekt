package de.ostfalia.learncoaching.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "Anliegen")
public class Anliegen {

	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idAnliegen")
	private Integer idAnliegen;

	@Column(name = "name")
	private String name;

	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@Column(name = "Kunde_idKunde")
	private Integer kunde_idKunde;

	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@Column(name = "Protokoll_idProtokoll")
	private Integer protokoll_idProtokoll;

	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	@Column(name = "jahr")
	private Integer jahr;

	public Integer getIdAnliegen() {
		return idAnliegen;
	}

	public void setIdAnliegen(Integer idAnliegen) {
		this.idAnliegen = idAnliegen;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getKunde_idKunde() {
		return kunde_idKunde;
	}

	public void setKunde_idKunde(Integer kunde_idKunde) {
		this.kunde_idKunde = kunde_idKunde;
	}

	public Integer getProtokoll_idProtokoll() {
		return protokoll_idProtokoll;
	}

	public void setProtokoll_idProtokoll(Integer protokoll_idProtokoll) {
		this.protokoll_idProtokoll = protokoll_idProtokoll;
	}

	public Integer getJahr() {
		return jahr;
	}

	public void setJahr(Integer jahr) {
		this.jahr = jahr;
	}
}
