package de.ostfalia.learncoaching;

import de.ostfalia.learncoaching.entity.*;
import de.ostfalia.learncoaching.repository.*;
import de.ostfalia.learncoaching.request.KundeRequest;
import de.ostfalia.learncoaching.request.ProtokollRequest;
import de.ostfalia.learncoaching.request.Statistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.sql.Date;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.List;

@RestController
public class LearnCoachingApi {

	@Autowired
	KundenDatenRepository kundenDatenRepository;
	@Autowired
	ErfahrenDurchRepository erfahrenDurchRepository;
	@Autowired
	ErfahrungenRepository erfahrungenRepository;
	@Autowired
	ProtokollRepository protokollRepository;
	@Autowired
	AnliegenRepository anliegenRepository;
	@Autowired
	LogInRepository logInRepository;

	@CrossOrigin
	@PostMapping("/create")
	void createKundenDaten(@RequestBody KundeRequest kundeRequest) {
		ErfahrenDurch erfahrenDurch1 = kundeRequest.getErfahrenDurch();
		erfahrenDurch1.setIdErfahrenDurch(null);
		ErfahrenDurch erfahrenDurch = erfahrenDurchRepository.save(erfahrenDurch1);
		Erfahrungen erfahrungen1 = kundeRequest.getErfahrungen();
		erfahrungen1.setIdErfahrungen(null);
		Erfahrungen erfahrungen = erfahrungenRepository.save(erfahrungen1);
		Kunde kunde = kundeRequest.getKundendaten();
		kunde.setIdKunde(null);
		kunde.setJahr(LocalDateTime.now().getYear());
		kunde.setLetzterEintrag(new Date(System.currentTimeMillis()));
		kunde.setErfahrenDurch_idErfahrenDurch(erfahrenDurch.getIdErfahrenDurch());
		kunde.setErfahrungen_idErfahrungen(erfahrungen.getIdErfahrungen());
		kundenDatenRepository.save(kunde);
	}

	@GetMapping("/getByName/{vorName}/{nachName}")
	public int findByName(@PathVariable String vorName, @PathVariable String nachName) {
		return kundenDatenRepository.findByVorNameAndNachName(vorName, nachName).getIdKunde();
	}

	@CrossOrigin
	@PostMapping("/addProtokoll/{vorName}/{nachName}")
	void addProtokoll(@RequestBody ProtokollRequest protokollRequest, @PathVariable String vorName,
			@PathVariable String nachName) {

		Integer kundeId = kundenDatenRepository.findByVorNameAndNachName(vorName, nachName).getIdKunde();
		kundenDatenRepository.findById(kundeId).get();
		Protokoll protokoll1 = protokollRequest.getProtokoll();
		protokoll1.setIdProtokoll(null);
		protokoll1.setJahr(LocalDateTime.now().getYear());
		protokoll1.setDatum(protokollRequest.getProtokoll().getDatum());
		protokoll1.setIdKunde(kundeId);

		Protokoll protokoll = protokollRepository.save(protokoll1);
		List<Anliegen> anliegenList = protokollRequest.getAnliegen();
		for (Anliegen anliegen : anliegenList) {
			anliegen.setIdAnliegen(null);
			anliegen.setJahr(LocalDateTime.now().getYear());
			anliegen.setKunde_idKunde(kundeId);
			anliegen.setProtokoll_idProtokoll(protokoll.getIdProtokoll());
			anliegenRepository.save(anliegen);
		}
	}

	@CrossOrigin
	@GetMapping("/getStatistics/{jahr}")
	Statistics getStatistics(@PathVariable("jahr") Integer jahr) throws ParseException {
		Statistics statistics = new Statistics();
		long numberOfApp = kundenDatenRepository.countByJahr(jahr);
		statistics.setKundenAnzahl(numberOfApp);

		long male = kundenDatenRepository.countByGeschlechtAndJahr("m", jahr);
		statistics.setManner(male);

		long female = kundenDatenRepository.countByGeschlechtAndJahr("f", jahr);
		statistics.setFrauen(female);

		long anzahlBeratungen = protokollRepository.countByJahr(jahr);
		statistics.setAnzahlBeratungen(anzahlBeratungen);

		long averageAge = kundenDatenRepository.averageAge(jahr);
		statistics.setDurchnittsalter(averageAge);

		List<Statistics.AnzhalBeratungenProKunde> protokollPerKundePerYear = protokollRepository
				.findProtokollPerKundePerYear(jahr);
		statistics.setAnzhalBeratungenProKunde(protokollPerKundePerYear);

		List<Statistics.AnliegenStatistics> anliegenPerYear = anliegenRepository.findAnliegenPerYear(jahr);
		statistics.setAnliegen(anliegenPerYear);
		return statistics;
	}

	@GetMapping("/getAll")
	public List<Protokoll> getName() {
		return protokollRepository.findAll();
	}

	@DeleteMapping("/deleteAll")
	public String deleteAll() {

		erfahrenDurchRepository.deleteAll();
		erfahrungenRepository.deleteAll();
		return "done";
	}

	@CrossOrigin
	@PutMapping("/deleteKunde/{vorName}/{nachName}")
	public void deleteKunde(@PathVariable String vorName, @PathVariable String nachName) {
		int idKunde;
		try {
			idKunde = kundenDatenRepository.findByVorNameAndNachName(vorName, nachName).getIdKunde();
			protokollRepository.delete(idKunde);
			kundenDatenRepository.deleteKunde(vorName, nachName);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@CrossOrigin
	@GetMapping("/getTable/{vorName}/{nachName}")
	public String[] sendTable(@PathVariable String vorName, @PathVariable String nachName) {
		try {
			Integer idKunde = kundenDatenRepository.findByVorNameAndNachName(vorName, nachName).getIdKunde();
			String[] table = protokollRepository.getProtokollen(idKunde);
			return table;
		} catch (Exception e) {
		}
		String[] s = { "false" };
		return s;
	}

	@CrossOrigin
	@PostMapping("/checkLogIn")
	public boolean checkLogIn(@RequestBody LogIn logIn) {
		String userName = logIn.getBenutzername();
		String password = logIn.getPasswort();
		if (logInRepository.countByBenutzernameAndPasswort(userName, password) == 1) {
			return true;
		} else {
			return false;
		}
	}

	@PostMapping("/addUser")
	public String addUser(@RequestParam String benutzer, @RequestParam String password) {
		int benutzerAnzahl = logInRepository.countByBenutzername(benutzer);
		if (benutzerAnzahl != 0) {
			return " The username is already exist, please chose another one!";
		} else {
			logInRepository.addUser(benutzer, password);
			return "The user has been added";
		}
	}

	@GetMapping("/deleteUser")
	public String deleteUser(@RequestParam String benutzer, @RequestParam String password) {
		if (logInRepository.countByBenutzernameAndPasswort(benutzer, password) == 0) {
			return " The user is not exist";
		} else {
			logInRepository.deleteUser(benutzer, password);
			return "The user has been deleted";
		}
	}

}
