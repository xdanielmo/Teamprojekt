### API documentation
http://localhost:8080/swagger-ui.html

### How to call create kunde API
http://localhost:8080/create

{
"erfahrenDurch": {
"idErfahrenDurch": 0,
"fyler": 1,
"rundmail": 0,
"vorstellung": 0,
"internet": 1,
"vortrag": 0,
"kommilitoinnen": 0,
"sonstiges": "This is sonstiges"
},
"erfahrungen": {
"allgemeine": 0,
"fachliche": 0,
"psychotherapeutische": 0,
"soziale": 0,
"career": 1,
"nichtGenannt": 0
},
"kundendaten": {
"vorName": "Mohd",
"nachName": "Odeh",
"alter": 30,
"wohnort": "Berlin",
"email": "odeh@email.com",
"telefon": "061234567",
"studiengang": "3",
"regelstudienzeit": 3,
"geschlecht": "male",
"jahr": 2022,
"bisherigeErfahrungen": "",
"letzterEintrag": "2022-05-24T23:21:51.083Z"
}
}

### How to call create protokoll API
http://localhost:8080/addProtokoll/1

{
"protokoll": {
"sonstiges": "blah blah",
"datum": "2022-05-25T00:00:46.296Z",
"jahr": 2022
},
"anliegen": [
{
"name": "anliegen1"
},
{
"name": "anliegen2"
}
]
}

