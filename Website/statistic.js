// variablen
let numberOfMale;
let numberOfFemale;
let numberOfDivers;
let anzahlkunde;
let durchschnittlichsalter;
let anzahlberatungen;
let anzahlberatungprokunde =[];
let anliegen = [];
let year;


// Objekte in html
let numberOfMaleOutput;
let numberOfFemaleOutput;
let numberOfDiversOutput;
let anzahlkundeOutput;
let durchschnittlichsalterOutput;
let anzahlberatungenOutput;
let anzahlberatungprokundeOutput;
let anliegenOutput;


function onChange(){
    numberOfMale = numberOfMaleOutput.value;
    numberOfFemale = numberOfFemaleOutput.value;
    numberOfDivers = numberOfDiversOutput;
    anzahlkunde = anzahlkundeOutput.value;
    durchschnittlichsalter = durchschnittlichsalterOutput.value;
    anzahlberatungen = anzahlberatungenOutput.value;
    anzahlberatungprokunde = anzahlberatungprokundeOutput.value;
    anliegen = anliegenOutput.value;
}

onload = function(){
    var yearSelect = document.getElementById("yearSelect");
    genarateYear();
    function genarateYear(){
        var currentYear = new Date().getFullYear();
        for(var i = currentYear; i > currentYear - 150; i--){
            var option = document.createElement("option");
            option.value = i;
            option.innerHTML = i;
            yearSelect.appendChild(option);
        }
    }
}

$('#btn-result').click(function (){
    var  yearchoose = '';
    yearchoose =
        $('#yearSelect').find('option')(function() {
            return !this.selected;
        }).data('year')
    year = yearchoose;
});

const getStatistik = async() =>{
    // year = 2022;
    var obj;
    const statistikJSON = await fetch('http://localhost:8080/getStatistics/'+year);
    const data = await statistikJSON.json();

    numberOfMale = data.manner;
    numberOfFemale = data.frauen;
    numberOfDivers = data.divers;
    anzahlkunde = data.kundenAnzahl;
    durchschnittlichsalter = data.durchnittsalter;
    anzahlberatungen = data.anzahlBeratungen;
    anzahlberatungprokunde = data.anzhalBeratungenProKunde;
    anliegen = data.anliegen;
    setValues();
    console.log(data);
}


function table() {
    anliegen.forEach(element => {

        var tr = document.createElement('tr');
  
        Object.values(element).forEach(text => {
          var td = tr.appendChild(document.createElement('td'));
          td.innerHTML = text;
        })
  
        document.getElementById("anliegenTable").appendChild(tr);
      });
}
function tableKunde() {
    anzahlberatungprokunde.forEach(element => {

        var tr = document.createElement('tr');
  
        Object.values(element).forEach(text => {
          var td = tr.appendChild(document.createElement('td'));
          td.innerHTML = text;
        })
  
        document.getElementById("kundenTable").appendChild(tr);
      });
}

var db = openDatabase("NameOfDatabase", "1.0", "itemDB", 65535)


db.close();


function setValues(){
    document.getElementById("männer").textContent=numberOfMale;
    document.getElementById("frauen").textContent=numberOfFemale;
    document.getElementById("divers").textContent=numberOfDivers;
    document.getElementById("alter").textContent=durchschnittlichsalter;
    document.getElementById("anzahlKunde").textContent=anzahlkunde;
    document.getElementById("anzahlBeratungen").textContent=anzahlberatungen;
    table();
    tableKunde();
}


