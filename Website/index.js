// Wird beim Laden der Seite ausgeführt
onload = (event) => {
  firstNameInput = document.getElementById('firstNameField');
  lastNameInput = document.getElementById('lastNameField');
  homeInput = document.getElementById('homeField');
  ageInput = document.getElementById('ageField');
  emailInput = document.getElementById('emailField');
  teleInput = document.getElementById('teleField');
  degreeInput = document.getElementById('degreeField');
  fachsemesterInput = document.getElementById('fachsemesterField');
  studiensemesterInput = document.getElementById('studiensemesterField');
  personalAdviseInput = document.getElementById('personalAdviseField');
  otherInfos1Input = document.getElementById('otherInfos1Field');
  otherInfos2Input = document.getElementById('otherInfos2Field');

  searchLine = document.getElementById("protocolNameInput");
  protocolsHeadline = document.getElementById("protocolsHeadline");
  addProtocolHeadline = document.getElementById("addProtocolHeadline");
  anliegenInput = document.getElementById("anliegen");
  infosProtocolInput = document.getElementById("infosProtocolInput");
  dateInput = document.getElementById("dateInput");

  bnameInput = document.getElementById("bname");
  passwordInput = document.getElementById("passwd");
}


//===============================================================================
// Log In

let bnameInput;
let bname;

let passwordInput;
let password;

let loggedIn = false;

function OnChangeL() {
  bname = bnameInput.value;
  password = passwordInput.value;
}

// Leitet einen zur richtigen Webseite weiter, wenn der Benutzer gefunden wurde. Wird durch klicken auf ANmelden ausgeführt.
const LoggingIn= async ()=> {

  /*
  let check =  fetch("http://localhost:8080/checkLogIn",{
      method : 'POST',
      headers : {
       
      'Content-Type' : 'application/json; charset=UTF-8'
      },
      body:JSON.stringify ({
          "benutzername" : bname,
          "passwort" : password
      }
  )}).then(function(response){
      return response.json()
  }).then(function(data){
          if (data) {
              console.log("Anmeldung erfolgt");
              document.getElementById("loggedPage").style.display="inline";
              document.getElementById("loginPage").remove();
              loggedIn = true;
          }
          else {
              console.log("Anmeldung fehlgeschlagen");
          }
  })
  */

  // Zum Testen der LoginFunktion. Muss später entfernt werden.
  document.getElementById("loggedPage").style.display="inline";
  document.getElementById("loginPage").remove();
  loggedIn = true;
}



//===============================================================================
// First (Für Erstgespräch)

// variablen
let firstName;
let lastName;
let home;
let age;
let email;
let tele;
let gender;
let degree;
let fachsemester;
let studiensemester;
//let studyTime;
let personalAdvise;
let otherInfos1;
let otherInfos2;

// Objekte in html
let firstNameInput;
let lastNameInput;
let homeInput;
let ageInput;
let emailInput;
let teleInput;
let degreeInput;
let fachsemesterInput;
let studiensemesterInput;
let boxes1 = [];
let boxes2 = [];
let personalAdviseInput;
let otherInfos1Input;
let otherInfos2Input;

// Wird ausgeführt, wenn sich Eingaben in Seite ändern, z.B. von Eingabefeldern oder Auswahlboxen
// Holt Infos aus den Auswahlmenus und speichert sie in Listen
function onChange() {
firstName = firstNameInput.value;
lastName = lastNameInput.value;
home = homeInput.value;
age = ageInput.value;
email = emailInput.value;
tele = teleInput.value;
degree = degreeInput.value;
fachsemester = fachsemesterInput.value;
studiensemester = studiensemesterInput.value;

gender = document.querySelector('input[name="optionsGender"]:checked').value;
//studyTime = document.querySelector('input[name="optionsTime"]:checked').value;

const checkboxes1 = document.querySelectorAll('input[name="boxes1"]');
const num1 = checkboxes1.length;
boxes1 = [];
for (let i = 0; i < num1; i++) {
  if (boxes1.indexOf(checkboxes1[i] !== -1)) {
    if (checkboxes1[i].checked === true) {
      boxes1.push(1);
    } else {
      boxes1.push(0);
    }
  }
}

personalAdvise = personalAdviseInput.value;
otherInfos1 = otherInfos1Input.value;

const checkboxes2 = document.querySelectorAll('input[name="boxes2"]');
const num2 = checkboxes1.length;
boxes2 = [];
for (let i = 0; i < num2; i++) {
  if (boxes2.indexOf(checkboxes2[i] !== -1)) {
    if (checkboxes2[i].checked === true) {
      boxes2.push(1);
    } else {
      boxes2.push(0);
    }
  }
}
if (boxes2[5] === 1) {
  boxes2 = [];
  boxes2.push(0);
  boxes2.push(0);
  boxes2.push(0);
  boxes2.push(0);
  boxes2.push(0);
  boxes2.push(1);
}

otherInfos2 = otherInfos2Input.value;

console.log(boxes1, boxes2);
}


const sendKunde = document.querySelector('#sendKunde');

// Übermittelt die Daten an das Backend-Programm
const createKunde = async() =>{
  if (loggedIn) {
    let kunde = await fetch('http://localhost:8080/create',{
      method : 'POST',
      headers: {
        'Acceept' : 'application/json',
        'Content-Type' : 'application/json'
      },
      body: JSON.stringify({
        erfahrenDurch : { "fyler": boxes1[0],
                          "rundmail": boxes1[2],
                          "vorstellung": boxes1[4],
                          "internet": boxes1[1],
                          "vortrag": boxes1[3],
                          "kommilitoinnen": boxes1[5],
                          "sonstiges": otherInfos1 
                        },
        erfahrungen : { "allgemeine": boxes2[0],
                        "fachliche": boxes2[2],
                        "psychotherapeutische": boxes2[4],
                        "soziale": boxes2[1],
                        "career": boxes2[3],
                        "nichtGenannt": boxes2[5],
                        "sonstiges": otherInfos2
                      },
    
        kundendaten : {vorName : firstName,
          nachName : lastName,
          alter : age,
          email : email,
          telefon : tele,
          studiengang : degree,
          semester : studiensemester,
          regelstudienzeit : fachsemester,
          //regelstudienzeit: studyTime,
          geschlecht : gender,
          //persoenlicherHinweis : personalAdvise,
          wohnort : home,
        },
        
        
      })
    })
    alert("Infos wurden abgeschickt.");
    console.log(gender);
  }
}







//==============================================================================================================
// Protocol (Für Protokollseite)

const sendProtokoll = document.querySelector('#sendProtokoll');

// variablen
let protocolVorname = "";
let protocolNachname = "";
let today = new Date();
let date;
let anliegen;
let infosProtocol;
let dateiSel;
let protkollTabelle=[];
let JSONTable;
// Objekte in html
let searchLine;
let protocolsHeadline;
let addProtocolHeadline;
let anliegenInput;
let infosProtocolInput;
let dateInput;
var fileByteArray = [];


// Lädt die ausgewählte Datei (für Protokoll hinzufügen)
var loadFile = function(event) {
dateiSel = document.getElementById('outputimg');
dateiSel.src = URL.createObjectURL(event.target.files[0]);
};

// Nimmt den Namen der Suchleiste und ändert Strings auf der Webseite für die Anzeige des Namens, führt außerdem loadProtocolTable() aus.
// Alle weiteren wörter als das Erste (Vorname) werden zusammen zum Nachnamen. Also bei Anton von und zu Papenburg wird "von und zu Papenburg" zum Nachnamen.
const findClient = async() => {
  if (loggedIn) {
    if(searchLine.value == ''){
      alert("The customer name is not correct!")
    }else{
        date = today.getFullYear()+"-"+(today.getMonth() + 1)+"-"+today.getDate();
        
        let protocolInput = searchLine.value;
        let names = protocolInput.split(' ');
        protocolVorname = names[0];
        protocolNachname = names[1];
        for (let i = 2; i < names.length; i++) {
          protocolNachname = protocolNachname + " " + names[i];
        }
        protkollTabelle = await fetch("http://localhost:8080/getTable/"+protocolVorname+"/"+protocolNachname);
            JSONTable = await protkollTabelle.json();
            if(JSONTable[0] == "false"){
              alert("there are no previous protocols for the customer!");
            }else{
            console.log("table " + JSONTable[0] + "just ");
           document.getElementById("protocols").style.display="inline";
            protocolsHeadline.textContent = "Protokolle von " + protocolVorname + " " + protocolNachname;
            document.getElementById("addProtocol").style.display="inline";
            addProtocolHeadline.textContent = "Neues Protokoll eintragen für " + protocolVorname + " " + protocolNachname + " am " + date;
            console.log(protocolVorname, protocolNachname);
          loadProtocolTable();
            }
      }
  }     
}

// Wird ausgeführt, wenn sich Eingaben in Seite ändern (Hier für die Protokollseite und nur für das Notizen Textfeld)
function onChangeP() {
infosProtocol = infosProtocolInput.value;

console.log(infosProtocol);
}

// Ermittelt alle ausgewählten Anliegen
function getAllAnliegen(vars) {
alleAnliegen = [];
for(var i = 0; i < vars.length; i++){
  if(vars[i].selected) {
    alleAnliegen.push(vars[i].value);
  }
}

anliegen = alleAnliegen;
console.log(anliegen);
}

// Aktualisiert den Wert des Datums, falls man manuell ein Datum in der Seite auswählt
function updateDate() {
date = dateInput.value;
console.log(date);
}

// Bestätigen des Löschens von Kunden, führt bei true zu deleteKunde()
const confirmDelete = async()=> {
let del = confirm('Möchten Sie die Daten von ' + protocolVorname + " " + protocolNachname + ' wirklich löschen? \n\nZum Löschen "OK" klicken \nUm nicht zu löschen "Abbrechen" klicken\n\n');
if (del == true) {
  deleteKunde();
}
}

// Löscht die bersonenbezogenen Daten aus der Datenbank (Statistisch wichtige Daten bleiben erhalten)
function deleteKunde (){
  if (loggedIn) {
    let Kunde = fetch('http://localhost:8080/deleteKunde/'+ protocolVorname + "/" + protocolNachname,{
      method : 'PUT',
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Acceept' : 'application/json',
        'Content-Type' : 'application/json'
      },
    })
  
  
  alert("Kundendaten von " + protocolVorname + " " + protocolNachname + " werden gelöscht...");
  }
}

/*
function display_image(src, width, height, alt) {
var a = document.createElement("img");
a.src = src;
a.width = width;
a.height = height;
a.alt = alt;
return a;
}*/

// Array für Protokolldaten
let protocolDatas = []


//Inserts the protocol data in the table dynamically
function loadProtocolTable() {
  
// Tabelle + Notizen + Date leeren
    tbl = document.getElementById("ptable");
    while(tbl.rows.length>0)
    {
      tbl.deleteRow(0);
    }
    infosProtocolInput.value = "";
    dateInput.value = "";

    let StringTable = JSONTable.toString();
    const TableArray = StringTable.split(',');

    
    // protkollTabelle in json zu array und in protocolDatas Liste
    for (i = 0; i < TableArray.length; i=i+3) {
      
      let obj = {date : TableArray[i],anliegen : TableArray[i+1], infos : TableArray[i+2]};
      protocolDatas.push(obj);
    }


    // Kopie der Daten erstellen und Reihenfolge für die Dartsellung in der Tabelle umkehren.
    let pDatas = [];
    for (i = 0; i < protocolDatas.length; i++) {
      pDatas[i] = protocolDatas[i];
    }
    pDatas.reverse();


    // Tabelle erzeugen
    pDatas.forEach(element => {

      var tr = document.createElement('tr');

      Object.values(element).forEach(text => {
        var td = tr.appendChild(document.createElement('td'));
        td.innerHTML = text;
      })

      document.getElementById("ptable").appendChild(tr);
    });
}

// Übermittelt die Daten des neuen Protokolls an das Backend-Programm
addProtokoll = async() =>{
let anliegenJson = [];
for(var i =0;  i < anliegen.length; i++){
  let a = {name : anliegen[i]};
  anliegenJson.push(a);
}

let protokoll = await fetch('http://localhost:8080/addProtokoll/'+ protocolVorname + "/" + protocolNachname,{    //Vorname und Nachname von Suchfeld
  method : 'POST',
  headers: {
    'Acceept' : 'application/json',
    'Content-Type' : 'application/json'
  },
  body:JSON.stringify({
    "protokoll": { "sonstiges": infosProtocol,
                  "datum" : date
                },
    "anliegen": anliegenJson      
   })
})
alert("Protokoll abgeschickt");
console.log(protokoll);
}
