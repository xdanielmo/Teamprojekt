package de.ostfalia.learncoaching.entity;

import de.ostfalia.learncoaching.entity.updateSqlData.Geschlecht;

public class Kunde {

    private final int id;
    private final int alter;
    private final updateSqlData.Geschlecht geschlecht;
    private final int idErfahrenDurch;
    private final int idErfahrungen;
    private boolean aufmerksamFlyer;
    private boolean aufmerksamRundmail;
    private boolean aufmerksamVorstellung;
    private boolean aufmerksamInternet;
    private boolean aufmerksamVortrag;
    private boolean aufmerksamKommilitonen;
    private boolean erfahrungAllgemein;
    private boolean erfahrungFachliche;
    private boolean erfahrungPsychotherapeutische;
    private boolean erfahrungSoziale;
    private boolean erfahrungCareer;
    private boolean erfahrungNichtGenannt;



    public Kunde(int id, int alter, updateSqlData.Geschlecht geschlecht, int idErfahrenDurch, int idErfahrungen) {
        this.id = id;
        this.alter = alter;
        this.geschlecht = geschlecht;
        this.idErfahrenDurch = idErfahrenDurch;
        this.idErfahrungen = idErfahrungen;
    }

    public int getId() {
        return id;
    }

    public int getAlter() {
        return alter;
    }

    public int getIdErfahrenDurch() {
        return idErfahrenDurch;
    }

    public int getIdErfahrungen() {
        return idErfahrungen;
    }

    public boolean isAufmerksamFlyer() {
        return aufmerksamFlyer;
    }

    public void setAufmerksamFlyer(boolean aufmerksamFlyer) {
        this.aufmerksamFlyer = aufmerksamFlyer;
    }

    public boolean isAufmerksamRundmail() {
        return aufmerksamRundmail;
    }

    public void setAufmerksamRundmail(boolean aufmerksamRundmail) {
        this.aufmerksamRundmail = aufmerksamRundmail;
    }

    public boolean isAufmerksamVorstellung() {
        return aufmerksamVorstellung;
    }

    public void setAufmerksamVorstellung(boolean aufmerksamVorstellung) {
        this.aufmerksamVorstellung = aufmerksamVorstellung;
    }

    public boolean isAufmerksamInternet() {
        return aufmerksamInternet;
    }

    public void setAufmerksamInternet(boolean aufmerksamInternet) {
        this.aufmerksamInternet = aufmerksamInternet;
    }

    public boolean isAufmerksamVortrag() {
        return aufmerksamVortrag;
    }

    public void setAufmerksamVortrag(boolean aufmerksamVortrag) {
        this.aufmerksamVortrag = aufmerksamVortrag;
    }

    public boolean isAufmerksamKommilitonen() {
        return aufmerksamKommilitonen;
    }

    public void setAufmerksamKommilitonen(boolean aufmerksamKommilitonen) {
        this.aufmerksamKommilitonen = aufmerksamKommilitonen;
    }

    public boolean isErfahrungAllgemein() {
        return erfahrungAllgemein;
    }

    public void setErfahrungAllgemein(boolean erfahrungAllgemein) {
        this.erfahrungAllgemein = erfahrungAllgemein;
    }

    public boolean isErfahrungFachliche() {
        return erfahrungFachliche;
    }

    public void setErfahrungFachliche(boolean erfahrungFachliche) {
        this.erfahrungFachliche = erfahrungFachliche;
    }

    public boolean isErfahrungPsychotherapeutische() {
        return erfahrungPsychotherapeutische;
    }

    public void setErfahrungPsychotherapeutische(boolean erfahrungPsychotherapeutische) {
        this.erfahrungPsychotherapeutische = erfahrungPsychotherapeutische;
    }

    public boolean isErfahrungSoziale() {
        return erfahrungSoziale;
    }

    public void setErfahrungSoziale(boolean erfahrungSoziale) {
        this.erfahrungSoziale = erfahrungSoziale;
    }

    public boolean isErfahrungCareer() {
        return erfahrungCareer;
    }

    public void setErfahrungCareer(boolean erfahrungCareer) {
        this.erfahrungCareer = erfahrungCareer;
    }

    public boolean isErfahrungNichtGenannt() {
        return erfahrungNichtGenannt;
    }

    public void setErfahrungNichtGenannt(boolean erfahrungNichtGenannt) {
        this.erfahrungNichtGenannt = erfahrungNichtGenannt;
    }
}
