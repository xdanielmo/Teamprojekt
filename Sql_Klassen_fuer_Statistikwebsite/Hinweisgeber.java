package de.ostfalia.learncoaching.entity;

public class Hinweisgeber {

    private String name;

    private int anzahl;

    public Hinweisgeber (String name, int anzahl){
        this.name = name;
        this.anzahl = anzahl;
    }

    public Hinweisgeber (String name){
        this.name = name;
        anzahl++;
    }



    public String getName() {
        return name;
    }

    public int getAnzahl() {
        return anzahl;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAnzahl(int anzahl) {
        this.anzahl = anzahl;
    }
}
