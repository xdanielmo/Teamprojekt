package de.ostfalia.learncoaching.entity;

public class Protokoll {

    private int anzahl;
    private final String anliegen;

    public Protokoll(String anliegen) {
        this.anzahl = 1;
        this.anliegen = anliegen;
    }

    public int getAnzahl() {
        return anzahl;
    }

    public String getAnliegen() {
        return anliegen;
    }

    public void setAnzahl(int anzahl) {
        this.anzahl = anzahl;
    }
}
