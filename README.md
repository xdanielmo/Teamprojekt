## Steps to run the backend app with MySql DB

### Run the DB:
1. Download and install MySql DB.
2. Connect to the DB --> Make sure that username and pass are both "root".
3. Create new schema "lerncoaching".

### Run the backend application:
1. Download and Install Intellij or eclipse.
2. Make sure to install JDK 17 in your machine.
3. Open the application 'learn-coaching' from the IDE (Intellij).
4. Run the application.